//
//  main.cpp
// updated by xue wang 8:52pm 10/30/14.
//  CacheCoherence
//
//  Created by Xinyue Huang on 10/30/14.
//  Copyright (c) 2014 Xinyue Huang. All rights reserved.
//

#include <iostream>
#define p 4 // core number
#define B 4

typedef struct request_type{
    int cycle;
    int core_no;
    bool RW;
    int Addr;
}request_t;

class request_queue{
public:
    int core_id;
    int sum_penalty;
    int last_request_finish_time;
    std::queue<request_t>;
    void insert_request();// insert request into queue
    request_t issue_request(); //issue ready request, return request, cycle = current_cycle>cycle?current_cycle:cycle
};

class cache_t{
public:
    int cache_id;
    int assoc;
    //LRU
    //DATA
    int Access_L1(int cache_id, int addr);
    void initializeCache(size, assoc, cache_id, level_below)
};

//preprocess the testbench
int preprocessAddr(){
    // input: testbench
    // output: cycle; core No; read/write; Addr
    // Access_L1(cycle, coreNumber, rw, Address);
    request_queue::insert_request(cycle, coreNumber, rw, Address);
}

//interface between L1 Caches and Cores:
int cache_t::Access_L1(int cache_id, int addr){//int cycle, int coreId, int rwTag, int Addr)
    // status: read(hit/miss) write(hit/miss)
    if(read hit) // do nothing
        return penalty =1;
    else// request to bus
        penalty += request_bus(int core_id, int status);
}
int cache_t::Access_L2(int cache_id, int addr){//int cycle, int coreId, int rwTag, int Addr)    
    if(hit):
}


int request_bus(int core_id, int status, int penalty){
    access_other_L1();
    if(other L1 serve){
        update_L1_status();
        update_memory_status();
        return penalty;
    }else{
        addr mod bank_no => which bank to access;
        access_L2();
        penalty += update_L1_status();
        update_memory_status();
        return penalty;
    }
}


//===================================================================================================
//                      main
//===================================================================================================
int main(int argc, const char * argv[]) {
    
    preprocessAddr();
    for(int i =0; i< p; i++){
        initializeCache(size, assoc, cache_id = p, level_below);//L1
    }
    initializeCache(size, assoc, cache_id = p+1, level_below);//L2
    initializeCache(size, assoc, cache_id = p+2, level_below);//memory
    
    
    
    while(cycle){
        for(int i =0; i< p; i++){// access every core
            request_t request = request_queue::issue_queue();
            int penalty =Access_L1(request);
            request_queue::last_request_finish_time = penalty + cycle;
            request_queue::sum_penalty += penalty;
        }
    }
    
    
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}